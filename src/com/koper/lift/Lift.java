package com.koper.lift;

import java.util.ArrayList;
import java.util.List;

public class Lift {
    private int actualFloor;
    private LiftDirection liftDirection;

    public Lift() { ;
        this.actualFloor=0;
        this.liftDirection = LiftDirection.STOPPED;
    }

    public int getActualFloor() {
        return actualFloor;
    }


    public LiftDirection getLiftDirection() {
        return liftDirection;
    }

    public void setLiftDirection(LiftDirection liftDirection) {
        this.liftDirection = liftDirection;
    }

    public void makeMove(LiftDirection moveDirection) {
        actualFloor+=moveDirection.getCodeNumber();
        liftDirection = moveDirection;
        System.out.println("I have arrived on "+actualFloor+" floor");
    }

    @Override
    public String toString() {
        return "Lift{" +
                "actualFloor=" + actualFloor +
                ", liftDirection=" + liftDirection +
                '}';
    }
}
