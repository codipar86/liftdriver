package com.koper.lift;

public enum LiftDirection {
    UP (1),
    DOWN (-1),
    STOPPED (0);

    private int codeNumber;

    LiftDirection(int codeNumber) {
        this.codeNumber = codeNumber;
    }

    public int getCodeNumber() {
        return codeNumber;
    }
}
