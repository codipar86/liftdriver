package com.koper.lift;

import java.util.ArrayList;
import java.util.List;

public class Building {
    private List<Floor> listOfFloors;

    public Building() {
        this.listOfFloors = new ArrayList<>();
    }
    public void addFloor(Floor floor) {
        listOfFloors.add(floor);
    }

    public List<Floor> getListOfFloors() {
        return listOfFloors;
    }
}
