package com.koper.lift;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ConcreteLiftDriver implements Mediator{
    List<Request> listOfAllRequests;
    Lift lift;
    Building building;


    public ConcreteLiftDriver() {
        this.listOfAllRequests = new ArrayList<>();
    }
    public void addLift(Lift lift) {
        this.lift = lift;
    }
    public void addBuilding(Building building) {
        this.building = building;
    }

    @Override
    public void addRequest(List<Request> listOfRequests) {
        this.listOfAllRequests.addAll(listOfRequests);
    }
    public void addRequest(Request request) {
        this.listOfAllRequests.add(request);
    }
    public void printRequests() {
        listOfAllRequests.forEach(System.out::println);
    }
    public void makeMove() {
        checkingActualFloorAndGettingRequests();
        movingUpOrDownAccordingToRequestsAndActualDirection();
        stoppingLiftIfThereAreNoMoreRequests();
    }

    private void stoppingLiftIfThereAreNoMoreRequests() {
        if (listOfAllRequests.isEmpty()) {
            lift.setLiftDirection(LiftDirection.STOPPED);
        }
    }

    private void movingUpOrDownAccordingToRequestsAndActualDirection() {
        if (listOfAllRequests.isEmpty()) {
            return;
        }
        List<Request> listOfAllUpstairsRequests = listOfAllRequests.stream()
                .filter(x->lift.getActualFloor()-x.getRequestedFloor()<0)
                .collect(Collectors.toList());
        List<Request> listOfAllDownstairsRequests = listOfAllRequests.stream()
                .filter(x->lift.getActualFloor()-x.getRequestedFloor()>0)
                .collect(Collectors.toList());
        if (lift.getLiftDirection().equals(LiftDirection.UP) && !listOfAllUpstairsRequests.isEmpty()) {
            lift.makeMove(LiftDirection.UP);
        } else if (lift.getLiftDirection().equals(LiftDirection.DOWN) && !listOfAllDownstairsRequests.isEmpty()) {
            lift.makeMove(LiftDirection.DOWN);
        } else if (lift.getLiftDirection().equals(LiftDirection.STOPPED) && !listOfAllUpstairsRequests.isEmpty() && !listOfAllDownstairsRequests.isEmpty()) {
            int nearestUpstairsRequestFloorDifference = listOfAllUpstairsRequests.stream()
                    .mapToInt(x->Math.abs(lift.getActualFloor()-x.getRequestedFloor()))
                    .min()
                    .getAsInt();
            int nearestDownstairsRequestFloorDifference = listOfAllDownstairsRequests.stream()
                    .mapToInt(x->Math.abs(lift.getActualFloor()-x.getRequestedFloor()))
                    .min()
                    .getAsInt();
            if (nearestUpstairsRequestFloorDifference<nearestDownstairsRequestFloorDifference) {
                lift.makeMove(LiftDirection.UP);
            } else {
                lift.makeMove(LiftDirection.DOWN);
            }

        } else if (lift.getLiftDirection().equals(LiftDirection.STOPPED) && listOfAllUpstairsRequests.isEmpty()) {
            lift.makeMove(LiftDirection.DOWN);
        } else if (lift.getLiftDirection().equals(LiftDirection.STOPPED) && listOfAllDownstairsRequests.isEmpty()) {
            lift.makeMove(LiftDirection.UP);
        } else if (lift.getLiftDirection().equals(LiftDirection.UP) && !listOfAllRequests.isEmpty()) {
            lift.makeMove(LiftDirection.DOWN);
        } else if (lift.getLiftDirection().equals(LiftDirection.DOWN) && !listOfAllRequests.isEmpty()) {
            lift.makeMove(LiftDirection.UP);
        }
    }

    private void checkingActualFloorAndGettingRequests() {
        Request actualLiftFloorRequest = new Request(lift.getActualFloor());
        if (listOfAllRequests.contains(actualLiftFloorRequest)) {
            for (Floor floor : building.getListOfFloors()) {
                if (floor.getFloorNumber()==lift.getActualFloor()) {
                    floor.sendRequestsFromFloor();
                }
            }
            listOfAllRequests.remove(actualLiftFloorRequest);
        }
    }
}
