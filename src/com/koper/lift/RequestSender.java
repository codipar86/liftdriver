package com.koper.lift;

public interface RequestSender {
    void sendRequest();
}
