package com.koper.lift;

import java.util.List;

public interface Mediator {

    void addRequest(List<Request> listOfRequests);
}
