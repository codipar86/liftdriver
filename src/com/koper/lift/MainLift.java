package com.koper.lift;

import java.util.Scanner;

public class MainLift {
    public static void main(String[] args) {

    ConcreteLiftDriver liftDriver = new ConcreteLiftDriver();
    Lift lift1 = new Lift();
    Building empireStateBuilding = new Building();

    liftDriver.addLift(lift1);
    liftDriver.addBuilding(empireStateBuilding);

    Floor floor0 = new Floor(0, liftDriver);
    Floor floor1 = new Floor(1, liftDriver);
    Floor floor2 = new Floor(2, liftDriver);
    Floor floor3 = new Floor(3, liftDriver);
    Floor floor4 = new Floor(4, liftDriver);

    empireStateBuilding.addFloor(floor0);
    empireStateBuilding.addFloor(floor1);
    empireStateBuilding.addFloor(floor2);
    empireStateBuilding.addFloor(floor3);
    empireStateBuilding.addFloor(floor4);




    Thread liftIsMoving = new Thread(() -> {
        while(true) {
            liftDriver.makeMove();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    });
    Thread getRequest = new Thread(() -> {
        while(true) {
            Scanner scanner = new Scanner (System.in);
            System.out.println("Enter floor number where You are waiting (0-4): ");
            int floorNumber = scanner.nextInt();
            for (int i=0; i<empireStateBuilding.getListOfFloors().size(); i++) {
                if (empireStateBuilding.getListOfFloors().get(i).getFloorNumber()==floorNumber) {
                    empireStateBuilding.getListOfFloors().get(i).addRequestToRequestsFromFloor(addRequestFromConsole(scanner));
                }
            }
        }

    });
    liftIsMoving.start();
    getRequest.start();

    }
    public static Request addRequestFromConsole(Scanner scanner) {

        System.out.println("Enter floor You want to reach(0-4): ");
        int requestedFloorNumber = scanner.nextInt();
        return new Request(requestedFloorNumber);
    }
}
