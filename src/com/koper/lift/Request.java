package com.koper.lift;

public class Request implements Comparable{
    private int requestedFloor;

    public Request(int requestedFloor) {
        this.requestedFloor = requestedFloor;
    }

    public int getRequestedFloor() {
        return requestedFloor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Request request = (Request) o;

        return requestedFloor == request.requestedFloor;
    }

    @Override
    public int hashCode() {
        return requestedFloor;
    }

    @Override
    public String toString() {
        return "Request{" +
                "requestedFloor=" + requestedFloor +
                '}';
    }

    @Override
    public int compareTo(Object re) {
        Request request = (Request) re;
        return requestedFloor-request.getRequestedFloor();
    }
}
