package com.koper.lift;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

public class Floor implements RequestSender{
    private int floorNumber;
    private Request floorRequest;
    private List<Request> listOfRequestsFromFloor;
    private ConcreteLiftDriver liftDriver;

    public Floor(int floorNumber, ConcreteLiftDriver liftDriver) {
        this.floorNumber = floorNumber;
        this.floorRequest = new Request(floorNumber);
        this.listOfRequestsFromFloor = new ArrayList<>();
        this.liftDriver = liftDriver;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    @Override
    public void sendRequest() {
        liftDriver.addRequest(floorRequest);
    }
    public void sendRequestsFromFloor() {
        liftDriver.addRequest(listOfRequestsFromFloor);
        deleteRequestsFromFloor();
    }
    public void addRequestToRequestsFromFloor(Request request) {
        listOfRequestsFromFloor.add(request);
        sendRequest();
    }
    private void deleteRequestsFromFloor() {
        listOfRequestsFromFloor.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Floor floor = (Floor) o;

        return floorNumber == floor.floorNumber;
    }

    @Override
    public int hashCode() {
        return floorNumber;
    }
}
